# Rush 01 – Piscine Python for Data Science

###### Food and nutrition


*Summary: This rush will help you to strengthen the skills acquired in the previous days.*



## Contents

1. [Preamble](#preamble)  
2. [Instructions](#instructions)
3. [Specific instructions of the day](#specificinstructionsoftheday)
4. [Mandatory part](#mandatory-part)

    4.1. [Main program](#main-program)

    4.2. [Development](#development)

    [a.Class Forecast](#aclass-forecast)	 

    [b.Class NutritionFacts](#bclass-nutritionfacts)	 

    [c.Class SimilarRecipes](#cclass-similarrecipes)	

    4.3. [Research](#research)	 

7. [Bonus part](#bonus-part)	 
8. [Turn-in and peer-evaluation](#turn-in-and-peer-evaluation)	 


<h2 id="chapter-i" >Chapter I</h2>
<h2 id="preamble" >Preamble</h2>


You are what you eat. Think about your body and organism as a system that needs to get many different supplements to live happily and effectively. If it lacks some nutrients, important reactions may stop happening and your health will eventually be weak. 

Your diet should be balanced if you want to live healthily. You need proteins, carbohydrates, and fats as well as Fe, Mg, Na, K, Ca, Zn, Se, Cr, I, vitamins D, B12, E, C, A, K, Cu, etc. It is already a long list but it is far away to be exhausted. If you do not eat enough proteins, the organism will start looking for them inside itself in muscles, organs, tissues. Eventually, you will lack enzymes, hormones, transport proteins, immune cells, etc. If you eat too much of them, the organism will be intoxicated by decay products and the health will decrease too. It applies to any item in the list. “Poison is in everything, and no thing is without poison. The dosage makes it either a poison or a remedy”.

The first problem is that our diet is not as diverse as it is required if we talk about an average person in the Western world. How often do you eat vegetables, fruits, nuts, beans, berries, bread, pastries, fish, meat, seaweed, butter? How often do you drink coffee, green tea, milk, alcohol, juices, smoothies?

The second problem is that we like to eat something tasty and all this healthy food is not as tasty as fried fries, chips, etc. The third problem is that we do not know good recipes.

What if we had an app that could predict how tasty a dish can be with the current products from our refrigerator, what good nutrients it contains and recommend us a menu with healthy and tasty recipes at the same time?


<h2 id="chapter-ii">Chapter II</h2>
<h2 id="instructions" >Instructions</h2>

- Use this page as the only reference. Do not listen to any rumors and speculations about how to prepare your solution.
- Here and further we use Python 3 as the only correct version of Python.
- The python files for python exercises (d01, d02, d03) must have a block in the end:

`if __name__ == '__main__'`.

- Pay attention to the permissions of your files and directories.
- To be assessed your solution must be in your GIT repository.
- Your solutions will be evaluated by your piscine mates.
- You should not leave in your directory any other file than those explicitly specified by the exercise instructions. It is recommended that you modify your .gitignore to avoid accidents.
- When you need to get precise output in your programs, it is forbidden to display a precalculated output instead of performing the exercise correctly.
- Have a question? Ask your neighbor on the right. Otherwise, try with your neighbor on the left.
- Your reference manual: mates / Internet / Google.
- Remember to discuss on the Intra Piscine forum.
- Read the examples carefully. They may require things that are not otherwise specified in the subject.
- And may the Force be with you!



<h2 id="chapter-iii">Chapter III</h2>
<h2 id="specificinstructionsoftheday">Specific instructions of the day</h2>

- No code in the global scope of the main program. Use the classes and methods that you have written in the development stage!
- Any exception not caught will invalidate the work, even in the event of an error that was asked you to test.
- The interpreter to use is Python 3.
- Any built-in function is allowed.




<h2 id="chapter-iv">Chapter IV</h2>
<h2 id="mandatory-part">Mandatory part</h2>

In this rush, you are going to work on your own application that will help people to eat healthier and tastier food. It is a good and valuable experience to create a product prototype out of different technologies that you have studied. That will be a nice result for just two weeks of diving in Python and data science!

Your work will have three stages: research (work in a Notebook), development (organizing everything in a module with classes and methods), and the program itself (python script). Each of the stages will produce corresponding files as a result.

<h2 id="main-program">Main program</h2>


Let us start from the end. The program is a Python script (`nutritionist.py`).

1. It takes in a list of ingredients.
2. It forecasts and returns the rating class (`bad`, `so-so`, `great`) of a potential dish with the ingredients.
3. It finds and returns all the nutrients (proteins, fats, sodium, etc.) in the ingredients as well as their daily values in %.
4. It finds 3 of the most similar recipes to the list of ingredients, their ratings, and the URLs where a user can find the full details.

The example is below:

```
$ ./nutritionist.py milk, honey, jam

I. OUR FORECAST

You might find it tasty, but in our opinion, it is a bad idea to have a dish with that list of ingredients.

II. NUTRITION FACTS

Milk

Protein – 6% of Daily Value

Total Carbohydrate – 1% of Daily Value

Total Fat – 1% of Daily Value

Calcium – 12% of Daily Value

…

Honey

…

Jam

…

III. TOP-3 SIMILAR RECIPES:

\- Strawberry-Soy Milk Shake, rating: 3.0, URL: https://www.epicurious.com/recipes/food/views/strawberry-soy-milk-shake-239217

\- …

\- …
```

<h2 id="development">Development</h2>

You need to create a Python module (`recipes.py`) with the classes and methods that are used in the main script.

<h2 id="aclass-forecast">a.Class Forecast</h2>

```
class Forecast:

"""

*Predicting the rating or the class*

"""

def __init__(self, list_of_ingredients):

"""

*Put here any fields that you think you will need.*

"""



def preprocess(self):

"""

*The method transforms the list of ingredients to the data structure that is used in machine learning algorithms for predictions.*

"""

return vector



def predict_rating(self):

"""

*The method returns the rating for the list of ingredients using the regression model that you trained upfront. Besides the rating itself, the method returns a text that interprets the rating and gives a recommendation as in the example above.*

"""

return rating, text



def predict_rating_category(self):

"""

*The method returns the rating category for the list of ingredients using the classification model that you trained upfront. Besides the rating itself, the method returns a text that interprets the rating category and gives a recommendation as in the example above.*

"""

return rating_cat, text
```
<h2 id="bclass-nutritionfacts">b.Class NutritionFacts</h2>

```
class NutritionFacts:

"""

*Offering nutritional facts on given ingredients.*

"""

def __init__(self, list_of_ingredients):

"""

*Put here any fields that you think you will need.*

"""

def retrieve(self):

"""

*The method gets all the nutrient facts about the given ingredients from the file with pre-collected information. It returns any structure that you find useful.*

"""

return facts

def filter(self, must_nutrients, n):
"""

*The method selects from the nutrient facts only nutrients from the list of must\_nutrients (for example, from PDF-files below) and the top-n nutrients with the highest values of daily value norms for the given ingredient. It returns a text formatted as in the example above.*

"""

return text_with_facts
```
<h2 id="cclass-similarrecipes">c.Class SimilarRecipes</h2>

```
class SimilarRecipes:

"""

*Recommending similar recipes with additional information*

"""

def __init__(self, list_of_ingredients):

"""

*Put here any fields that you think you will need.*

"""    

def find_all(self):

"""

*The method returns a list of indexes of the recipes that contain the given list of ingredients. If there is no recipe that contains all the ingredients, handle it.*

"""

return indexes



def top_similar(self, n):

"""

*The method returns a text formatted as in the example above with title, rating, and URL. Before that, it finds top-n most similar recipes by the number of additional ingredients that are required in the recipes using indexes from the find_all method. The most similar is the one that does not require any other ingredients. Next is the one that requires only one, etc. If it takes more than 5 ingredients, do not return those recipes.*

"""

return text_with_recipes
```

<h2 id="research">Research</h2>

In this part of the rush, you need to prepare everything that is used in the classes and methods above in a Jupyter Notebook (recipes.ipynb).

1. Forecast

    a. Data preparation

    i. Use [the dataset](https://drive.google.com/file/d/1hzmxNBrY7-9mv5EpqAvhVUiJahfrcYUN/view?usp=sharing) from Epicurious collected by HugoDarwood.

    ii. Filter the columns: the less non-ingredient columns in your dataset the better. You will predict the rating or rating category using only the ingredients and nothing else.

    b. Regression

    i. Try different algorithms and their hyperparameters for rating prediction. Choose the best on cross-validation and find the score (RMSE) on the test subsample.

    ii. Try different ensembles and their hyperparameters. Choose the best on cross-validation and find the score on the test subsample.
      
    iii. Calculate the RMSE for a naive regressor that predicts the average rating.

    c. Classification
      
    i. Binarize the target column by rounding the ratings to the closest integer. It will be your classes.
     
    i. Try different algorithms and their hyperparameters for class prediction. Choose the best on cross-validation and find the score (accuracy) on the test subsample.
      
    iii. Compare the metrics using accuracy. Calculate the accuracy of a naive classificator that predicts the most common class.
      
    iv. Binarize again the target column by converting the integers to classes ‘bad’ (0, 1), ‘so-so’ (2, 3), ‘great’ (4, 5).
     
    v. Try different algorithms and their hyperparameters for class prediction. Choose the best on cross-validation and find the score on the test subsample.
      
    vi. Compare the metrics using accuracy. Calculate the accuracy of a naive classificator that predicts the most common class.
      
    vii. What is worse: to predict a bad rating which is good in real life, or to predict a good rating which is bad in real life? Replace accuracy with the appropriate metric.

    viii. Try different algorithms and their hyperparameters for class prediction with the new metric. Choose the best and find the score on the test subsample.

    ix. Try different ensembles and their hyperparameters. Choose the best and find the score on the test subsample.

    d. Decision

    i. Decide what is better to use: the regression model or the classification. Save the best model. You will use it in the program.

2. Nutrition Facts

    a. Collect all the nutrition facts for the ingredients from your prepared and filtered dataset (only ingredient columns) into a dataframe. Use [the following API](https://fdc.nal.usda.gov/api-guide.html) for that.

    b. Transform all the values into % of the daily value. Keep only ingredients that exist in [this](https://drive.google.com/file/d/1jn0t5tU_RgOpq4wcO-uS4D0_NAP6MwHz/view?usp=sharing) and [that](https://drive.google.com/file/d/1bmdZGB0QwND2BD3XlC1JswL7AdnTJHLT/view?usp=sharing) table.

    c. Save the transformed dataframe into a CSV file that you will use in your main program.

3. Similar Recipes

    a. For each recipe from the dataset, collect the URL from epicurious.com with the details (if there is no URL for that recipe, find a similar one on the Internet).

    b. Save the new dataframe to a CSV file that you will use in your main program.




<h2 id="chapter-v">Chapter V</h2>
<h2 id="bonus-part">Bonus part</h2>

Add to the classes more methods that will help the script perform a new function: generate a menu for a day. 

The day menu should randomly give a list of the three recipes that cover most of the needs in nutrients (% of the daily value) without overtaking them and have the highest total rating. You should offer only appropriate recipes for breakfast, lunch, and dinner. The result of the program should be like this:

BREAKFAST

---------------------

Feta, Spinach, and Basil Omelette Muffins (rating: 4.375)



Ingredients:

- arugula

- egg

- feta

- muffin

- omelet

- spinach

- tomato

Nutrients:

- calories: 7.5%

- protein: 16%

- fat: 10%

- sodium: 7 % 

- …



URL: https://www.epicurious.com/recipes/food/views/feta-spinach-and-basil-omelette-muffins

LUNCH

---------------------

… 



<h2 id="chapter-vi">Chapter VI</h2>
<h2 id="turn-in-and-peer-evaluation">Turn-in and peer-evaluation</h2>

Turn in your work using your git repository, as usual. Only the work that’s in your repository will be graded during the evaluation.

During the correction, you will be evaluated on your turn-in as well as your ability to present, explain, and justify your choices.


